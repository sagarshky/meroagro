<?php /* Template Name: front */ ?>

<?php get_header(); ?>
    <div class="main-container">
    <section class="hero-slider">
        <ul class="slides text-center">
            <?php

            $post_meta_array = get_post_meta($post->ID, "wpcf-sliderimage");

            foreach ($post_meta_array as $post_meta) {

                ?>
                <li class="overlay">
                    <div class="background-image-holder parallax-background">
                        <img class="background-image" alt="Background Image" src="<?= $post_meta; ?>">
                    </div>
                </li>
            <?php }

            ?>

        </ul>
        <div class="overlay-banner">
            <div class="text-center" style="width: 470px;">
                <img src="<?= get_template_directory_uri(); ?>/img/logo-agro.png" alt=""/>
                <div class="text-right">
                    <h1 style="margin-bottom: 8px">Mero Agro Pvt. Ltd.</h1>
                    <h5 style="background: #008131;
    display: initial;
    padding: 3px 5px;
    font-size: 28px;
    letter-spacing: 4px;
    color: #d3c32f;
    font-style: italic;">किसानको साथी </h5>
                </div>
            </div>
        </div>
    </section>
    <section class="image-divider duplicatable-content hero-services">
        <div class="background-image-holder parallax-background">
            <img class="background-image" alt="Background Image"
                 src="<?= get_template_directory_uri(); ?>/img/introduction_bg.jpg">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                    <!--                    <i class="icon icon-jumbo social_twitter"></i>-->
                    <h1>Products we serve</h1>
                    <h5>DISTRIBUTORS AND FARMERS across DIFFERENT LEVELs</h5>
                </div>
            </div>

            <div class="row">
                <div class="service-sectiona">


                    <?php

                    $categories = get_terms('product-cat', array(
                        'orderby' => 'count',
                        'hide_empty' => 0,
                        'parent' => 0
                    ));


                    foreach ($categories as $term) :


                        ?>


                        <div class="col-md-3">
                            <div id="hearo" class="box overlay">
                                <img src="<?php the_field('category_image', 'product-cat_' . $term->term_id); ?>"
                                     class="box-img"/>
                                <div class="overlay-box-content">
                                    <h1><?php echo $term->name; ?></h1>
                                    <!--                                    <p class="hidden">--><!--...</p>-->
                                    <?php

                                    $args = array(
                                        'post_type' => 'all-product',
                                        'tax_query' => array(
                                            array(
                                                'taxonomy' => 'product-cat',
                                                'posts_per_page' => 1,
                                                'field' => 'term_id',
                                                'terms' => $term->term_id
                                            ),
                                        ),
                                    );
                                    $query = get_posts($args);

                                    $post_id = $query[0]->ID;
                                    if ($term->term_id == 13) {
                                        $post_id = 149;
                                    }
                                    ?>
                                    <a href="<?php echo get_permalink($post_id); ?>"
                                       class="btn btn-primary btn-filled btn-xs">Read more</a>
                                </div>

                            </div>
                        </div>
                    <?php endforeach;
                    ?>
                </div>
            </div><!--end of row-->
        </div><!--end of container-->
    </section>

    <section class="hero-aboutagro overlay"
             style="background: url('<?= get_template_directory_uri(); ?>/uploads/2083_1475747887.jpg') no-repeat center top; background-size: coverq">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                    <h1>All About <span>mero agro</span></h1>
                    <?php
                    $id = 200;
                    $post = get_post($id);
                    $content = apply_filters('the_content', $post->post_content);
                    echo $content;
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                    <div id="hero" class="feature-video">
                        <div class="feature-main"
                             style="background: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>') center no-repeat; background-size: cover;">

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="hero-fromtheblog hidden">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h1>From the Blog</h1>
                    <p>A blog (a truncation of the expression weblog) is a discussion or informational site
                        published</p>
                    <a href="" class="btn btn-primary btn-filled btn-xs">Check our blog</a>
                </div>
                <div class="col-md-6 col-md-offset-1">
                    <div id="blog-items-scroller" class="owl-carousel" style="position: absolute; z-index: 99">
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/731_1475748801.jpg"
                                 alt="Self-Verifying Cocoon">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">Self-Verifying Cocoon</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/248915214_1475817771.png"
                                 alt="GrainPro SuperGrainbag Farm 50">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">GrainPro SuperGrainbag Farm 50</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/337942428_1475817787.jpg"
                                 alt="Collapsible Dryer Case II">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">Collapsible Dryer Case II</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/225814716_1475817822.jpg"
                                 alt="Solar Bubble Dryer">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">Solar Bubble Dryer</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/1006939258_1475817845.png"
                                 alt="Moisture Meter (MM4)">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">Moisture Meter (MM4)</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/640728207_1475817873.png"
                                 alt="Oxygen Meter">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">Oxygen Meter</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/558085045_1475817915.png"
                                 alt="Bullet">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">Bullet</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/1904959250_1475817943.png"
                                 alt="Rivan">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">Rivan</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/480600529_1475817974.png"
                                 alt="Diffence">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">Diffence</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/876112786_1475817709.png"
                                 alt="Real M-45">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">Real M-45</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/1583982939_1475818008.png"
                                 alt="RealBat">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">RealBat</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/1389733363_1475818042.png"
                                 alt="RealMax">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">RealMax</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/612683052_1475818103.jpg"
                                 alt="Ca-boron liqid">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">Ca-boron liqid</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/653123206_1475818178.jpg"
                                 alt="Crop Juice">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">Crop Juice</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/342230202_1475818275.jpg"
                                 alt="Seaweed 3000">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">Seaweed 3000</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/232381083_1475818645.jpg"
                                 alt="Vit tabs">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">Vit tabs</div>
                            </div>
                        </div>
                        <div class="blog-item">
                            <img class=" blog-feature-img"
                                 src="<?= get_template_directory_uri(); ?>/uploads/1210830828_1475818859.jpg"
                                 alt="RealStar">
                            <div class="blog-post">
                                <div class="blog-loc">,</div>
                                <div class="blog-title">RealStar</div>
                            </div>
                        </div>
                    </div><!-- End Single Product Carousel-->

                </div>
            </div>
        </div>
    </section>



<?php get_footer(); ?>